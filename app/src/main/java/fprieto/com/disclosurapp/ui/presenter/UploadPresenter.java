package fprieto.com.disclosurapp.ui.presenter;

import android.content.Context;

import javax.inject.Inject;

public class UploadPresenter extends Presenter<UploadPresenter.View> {

    @Inject
    public UploadPresenter() {
    }

    public interface View extends Presenter.View {
        Context context();
    }
}
