package fprieto.com.disclosurapp.ui.presenter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import fprieto.com.disclosurapp.ui.view.SavingTempImageListener;
import fprieto.com.disclosurapp.usecase.ExportImage;

public class UploadedPresenter extends Presenter<UploadedPresenter.View> {

    private ExportImage exportImage;

    @Inject
    public UploadedPresenter(@NonNull ExportImage exportImage) {
        this.exportImage = exportImage;
    }

    public void exportImage(@NonNull Bitmap bitmap, boolean isSaving, @NonNull SavingTempImageListener savingImageListener) {
        exportImage.exportTempImage(bitmap, isSaving, savingImageListener);
    }

    public interface View extends Presenter.View {
        Context context();

    }
}
