
package fprieto.com.disclosurapp;

import android.app.Application;

import fprieto.com.disclosurapp.di.ApplicationComponent;
import fprieto.com.disclosurapp.di.DaggerApplicationComponent;
import fprieto.com.disclosurapp.di.MainModule;

public class DisclosurappApplication extends Application {

    private static ApplicationComponent component;

    public static ApplicationComponent component() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .mainModule(new MainModule(this))
                .build();
    }
}
