package fprieto.com.disclosurapp.ui.view;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.yalantis.ucrop.view.UCropView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fprieto.com.disclosurapp.DisclosurappApplication;
import fprieto.com.disclosurapp.R;
import fprieto.com.disclosurapp.ui.presenter.UploadedPresenter;
import fprieto.com.disclosurapp.ui.view.adapter.FacePagerAdapter;
import fprieto.com.disclosurapp.ui.view.widget.MoveGestureDetector;
import fprieto.com.disclosurapp.ui.view.widget.RotateGestureDetector;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static fprieto.com.disclosurapp.R.drawable.face;
import static fprieto.com.disclosurapp.R.drawable.face_1;
import static fprieto.com.disclosurapp.R.drawable.face_2;
import static fprieto.com.disclosurapp.R.drawable.face_3;
import static fprieto.com.disclosurapp.R.drawable.face_4;
import static fprieto.com.disclosurapp.R.drawable.face_5;
import static fprieto.com.disclosurapp.R.drawable.face_6;


public class UploadedActivity extends BaseActivity implements View.OnTouchListener, UploadedPresenter.View, SavingTempImageListener, ItemSelectedListener {

    public static final String TAG = UploadedActivity.class.getCanonicalName();
    public static final int FLIP_VERTICAL = 1;
    public static final int FLIP_HORIZONTAL = 2;
    static final int MAX_DURATION = 500;
    private static final int DOWNLOAD_NOTIFICATION_ID_DONE = 911;
    private static final int DEFAULT_INDICATOR_OFFSET_VALUE = 20;
    private static final int TAKE_PICTURE = 3;
    private static final int RESULT_LOAD_IMAGE = 0;
    private static final boolean IS_SAVING = true;
    @BindView(R.id.ucrop)
    UCropView uCropView;
    @BindView(R.id.rl_content)
    RelativeLayout mContent;
    @BindView(R.id.vp_faces)
    ViewPager mViewPager;
    @BindView(R.id.iv_back)
    ImageView mBack;
    @BindView(R.id.iv_next)
    ImageView mNext;
    @BindView(R.id.iv_face)
    ImageView mFace;
    @BindView(R.id.v_frame)
    View mFrame;
    @BindView(R.id.ll_pager_bullet_indicator)
    LinearLayout mLayoutIndicator;
    @Inject
    UploadedPresenter presenter;
    private int mSelectedDrawableId;
    private int activeColorTint;
    private int inactiveColorTint;
    private int offset = DEFAULT_INDICATOR_OFFSET_VALUE;
    private int clickCount = 0;
    private long startTime;
    private long duration;
    private boolean pushed = false;
    private Matrix mMatrix = new Matrix();
    private float mScaleFactor = 2f;
    private float mRotationDegrees = 0.f;
    private float mFocusX = 0.f;
    private float mFocusY = 0.f;
    private int mAlpha = 255;
    private int mImageHeight, mImageWidth;
    private ScaleGestureDetector mScaleDetector;
    private RotateGestureDetector mRotateDetector;
    private MoveGestureDetector mMoveDetector;
    // private Subscription mEventBusSubscription;
    private FacePagerAdapter facePagerAdapter;
    private boolean mShowLoader = false;
    private boolean mShowDone = false;
    private boolean isSaving;


    public static void startWithUri(@NonNull Context context, @NonNull Uri uri) {
        Intent intent = new Intent(context, UploadedActivity.class);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static Drawable wrapTintDrawable(Drawable sourceDrawable, int color) {
        if (color != 0) {
            Drawable wrapDrawable = DrawableCompat.wrap(sourceDrawable);
            DrawableCompat.setTint(wrapDrawable, color);
            wrapDrawable.setBounds(0, 0, wrapDrawable.getIntrinsicWidth(),
                    wrapDrawable.getIntrinsicHeight());
            return wrapDrawable;

        } else {
            return sourceDrawable;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUCropView();
        initToolbar();
        initializeDagger();
        initializePresenter();
        initializeViewPager();
        presenter.initialize();
        ButterKnife.bind(this);
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        getMenuInflater().inflate(R.menu.menu_result, menu);
        // Change crop & loader menu icons color to match the rest of the UI colors

        MenuItem menuItemLoader = menu.findItem(R.id.menu_loader);
        Drawable menuItemLoaderIcon = menuItemLoader.getIcon();
        if (menuItemLoaderIcon != null) {
            try {
                menuItemLoaderIcon.mutate();
                menuItemLoaderIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorVeige), PorterDuff.Mode.SRC_ATOP);
                menuItemLoader.setIcon(menuItemLoaderIcon);
            } catch (IllegalStateException e) {
                Log.i(TAG, String.format("%s - %s", e.getMessage(), getString(com.yalantis.ucrop.R.string.ucrop_mutate_exception_hint)));
            }
            ((Animatable) menuItemLoader.getIcon()).start();
        }

        return true;
    }


    private void setStatusbarColor(boolean saved) {
        setStatusbar(saved ? R.color.status_bar_green : R.color.colorStatusBar);
    }

    private void setStatusbar(int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(color));
        }
    }

    private void showNotification(@NonNull File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri fileUri = FileProvider.getUriForFile(
                this,
                getString(R.string.file_provider_authorities),
                file);

        intent.setDataAndType(fileUri, "image/*");

        List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo info : resInfoList) {
            grantUriPermission(
                    info.activityInfo.packageName,
                    fileUri, FLAG_GRANT_WRITE_URI_PERMISSION | FLAG_GRANT_READ_URI_PERMISSION);
        }

        NotificationCompat.Builder mNotification = new NotificationCompat.Builder(this);

        mNotification
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_image_saved_click_to_preview))
                .setTicker(getString(R.string.notification_image_saved))
                .setSmallIcon(android.R.drawable.ic_notification_overlay)
                .setOngoing(false)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setContentIntent(PendingIntent.getActivity(this, 0, intent, 0))
                .setAutoCancel(true);

        NotificationManager notificationManager = ((NotificationManager) getSystemService(NOTIFICATION_SERVICE));
        notificationManager.notify(DOWNLOAD_NOTIFICATION_ID_DONE, mNotification.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(String.valueOf(DOWNLOAD_NOTIFICATION_ID_DONE), getString(R.string.notification_channel), NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(getString(R.string.notification_image_saved_click_to_preview));
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void shareImage(String path) {
        final File file = new File(path);
        MediaScannerConnection.scanFile(this, new String[]{path},
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(
                                android.content.Intent.ACTION_SEND);
                        shareIntent.setType("image/*");
                        shareIntent.putExtra(
                                android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
                        shareIntent.putExtra(
                                android.content.Intent.EXTRA_TITLE, getString(R.string.share_title));
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(Intent.createChooser(shareIntent,
                                getString(R.string.summary_share_text)));

                    }
                });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_download).setVisible(!mShowDone && !mShowLoader);
        menu.findItem(R.id.menu_loader).setVisible(!mShowDone && mShowLoader);
        menu.findItem(R.id.menu_ok).setVisible(mShowDone);
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_download) {
            saveImageTemporary(IS_SAVING);
            mShowLoader = true;
            supportInvalidateOptionsMenu();
        } else if (item.getItemId() == R.id.menu_share) {
            saveImageTemporary(!IS_SAVING);

        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(getIntent().getData().getPath()).getAbsolutePath(), options);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void initUCropView() {
        try {
            uCropView.getCropImageView().setImageUri(getIntent().getData(), null);
            uCropView.getOverlayView().setShowCropFrame(false);
            uCropView.getOverlayView().setShowCropGrid(false);
            uCropView.getOverlayView().setDimmedColor(Color.TRANSPARENT);
        } catch (Exception e) {
            Log.e(TAG, "setImageUri", e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initializeViewPager() {
        invalidateBullets();
        final ArrayList<Integer> listFacesResource = new ArrayList<Integer>();
        listFacesResource.add(face);
        listFacesResource.add(face_1);
        listFacesResource.add(face_2);
        listFacesResource.add(face_3);
        listFacesResource.add(face_4);
        listFacesResource.add(face_5);
        listFacesResource.add(face_6);


        facePagerAdapter = new FacePagerAdapter(this, listFacesResource, this);
        mViewPager.setAdapter(facePagerAdapter);
        invalidateBullets(facePagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setIndicatorItem(position);
                if (position == 0) {
                    mBack.setVisibility(View.GONE);
                } else if (position == listFacesResource.size() - 1) {
                    mNext.setVisibility(View.GONE);
                } else {
                    mBack.setVisibility(View.VISIBLE);
                    mNext.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageScrolled(int position, float arg1, int arg2) {
                if (arg1 == 0.0 && arg2 == 0) {
                    mFrame.setAlpha(1f);
                } else {
                    mFrame.setAlpha(arg1);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getVisibility() == View.GONE) {
            mViewPager.setVisibility(View.VISIBLE);
            mFace.setVisibility(View.GONE);
            mFrame.setVisibility(View.VISIBLE);
            mBack.setVisibility(View.VISIBLE);
            mNext.setVisibility(View.VISIBLE);
            mLayoutIndicator.setVisibility(View.VISIBLE);

        } else {
            super.onBackPressed();
        }

    }

    private void initializePresenter() {
        presenter.setView(this);
    }

    private void initializeImage() {
        mFace.setTag(mFace);
        mFace.setOnTouchListener(this);
        mMatrix = new Matrix();
        // Determine the center of the screen to center 'earth'
        Display display = getWindowManager().getDefaultDisplay();
        mFocusX = display.getWidth() / 2f;
        mFocusY = display.getHeight() / 2f;


        // Determine dimensions of 'earth' image
        Drawable d = ContextCompat.getDrawable(this, mSelectedDrawableId);
        mImageHeight = d.getIntrinsicHeight();
        mImageWidth = d.getIntrinsicWidth();

        // View is scaled and translated by matrix, so scale and translate initially
        float scaledImageCenterX = (mImageWidth * mScaleFactor) / 2;
        float scaledImageCenterY = (mImageHeight * mScaleFactor) / 2;

        mMatrix.postScale(mScaleFactor, mScaleFactor);
        mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY - scaledImageCenterY);
        mFace.setImageMatrix(mMatrix);

        // Setup Gesture Detectors
        mScaleDetector = new ScaleGestureDetector(getApplicationContext(), new ScaleListener());
        mRotateDetector = new RotateGestureDetector(getApplicationContext(), new RotateListener());
        mMoveDetector = new MoveGestureDetector(getApplicationContext(), new MoveListener());
    }

    @SuppressWarnings("deprecation")
    public boolean onTouch(View v, MotionEvent event) {
        mScaleDetector.onTouchEvent(event);
        mRotateDetector.onTouchEvent(event);
        mMoveDetector.onTouchEvent(event);

        mShowLoader = false;
        mShowDone = false;
        supportInvalidateOptionsMenu();
        setStatusbarColor(mShowDone);

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: // first finger down only
                //handle startTime click to perform double tap if necessary
                startTime = System.currentTimeMillis();
                clickCount++;
                break;
            case MotionEvent.ACTION_UP: // first finger lifted
                long time = System.currentTimeMillis() - startTime;
                duration = duration + time;
                if (clickCount == 2) {
                    if (duration <= MAX_DURATION) {
                        ((ImageView) v.getTag()).setImageBitmap(flipImage(BitmapFactory.decodeResource(
                                getResources(), mSelectedDrawableId), 2));
                    }
                    clickCount = 0;
                    duration = 0;
                }
                break;
        }


        float scaledImageCenterX = (mImageWidth * mScaleFactor) / 2;
        float scaledImageCenterY = (mImageHeight * mScaleFactor) / 2;

        mMatrix.reset();
        mMatrix.postScale(mScaleFactor, mScaleFactor);
        mMatrix.postRotate(mRotationDegrees, scaledImageCenterX, scaledImageCenterY);
        mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY - scaledImageCenterY);

        ImageView view = (ImageView) v;
        view.setImageMatrix(mMatrix);
        view.setAlpha(mAlpha);

        return true; // indicate event was handled
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void onImageSaved(boolean result, String path, boolean isSaving) {
        if (result) {
            if (isSaving) {
                mShowLoader = false;
                mShowDone = true;
                supportInvalidateOptionsMenu();
                setStatusbarColor(mShowDone);
                showNotification(new File(path));
            } else {
                shareImage(path);
            }
        }
    }

    @Override
    public void onItemSelected(int id) {
        mFrame.setVisibility(View.GONE);
        mLayoutIndicator.setVisibility(View.GONE);
        mBack.setVisibility(View.GONE);
        mNext.setVisibility(View.GONE);

        mSelectedDrawableId = id;
        initializeImage();
        mViewPager.setVisibility(View.GONE);
        mFace.setVisibility(VISIBLE);
        mFace.setImageDrawable(ContextCompat.getDrawable(this, mSelectedDrawableId));
    }

    @Override
    public void onPolaroidItemClicked() {

    }

    @Override
    public void onPolaroidItemChanged(int drawableId) {
    }

    public Bitmap flipImage(Bitmap src, int type) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        // if vertical
        if (type == FLIP_VERTICAL) {
            // y = y * -1
            matrix.preScale(1.0f, -1.0f);
        }
        // if horizonal
        else if (type == FLIP_HORIZONTAL) {
            // x = x * -1
            if (pushed == false) {
                matrix.preScale(-1.0f, 1.0f);
                pushed = true;
            } else {
                pushed = false;
                matrix.preScale(1.0f, 1.0f);
            }

            // unknown type
        } else {
            return null;
        }

        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(),
                matrix, true);
    }

    private void initializeDagger() {
        ((DisclosurappApplication) getApplication()).component().inject(this);
    }

    @OnClick({R.id.iv_back, R.id.iv_next})
    void click(ImageView arrow) {
        int currentItem = mViewPager.getCurrentItem();
        switch (arrow.getId()) {
            case R.id.iv_back:
                if (currentItem - 1 > 0) {
                    mViewPager.setCurrentItem(currentItem - 1);
                }
                break;
            case R.id.iv_next:
                if (currentItem + 1 < facePagerAdapter.getCount()) {
                    mViewPager.setCurrentItem(currentItem + 1);
                }
                break;
        }

    }

    private void saveImageTemporary(boolean isSaving) {

        this.isSaving = isSaving;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    getString(R.string.permission_write_storage_rationale),
                    REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
        } else {
            mContent.destroyDrawingCache();
            mContent.setDrawingCacheEnabled(true);
            presenter.exportImage(mContent.getDrawingCache(), isSaving, this);
            this.isSaving = false;
        }

    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveImageTemporary(isSaving);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_uploaded;
    }

    private void initIndicator(int count) {
        mLayoutIndicator.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        int margin = Math.round(getResources()
                .getDimension(R.dimen.pager_bullet_indicator_dot_margin));

        params.setMargins(margin, 0, margin, 0);
        Drawable drawableInactive = ContextCompat.getDrawable(this,
                R.drawable.inactive_dot);

        for (int i = 0; i < count; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setImageDrawable(drawableInactive);
            mLayoutIndicator.addView(imageView, params);
        }
    }

    public void invalidateBullets() {
        PagerAdapter adapter = mViewPager.getAdapter();
        if (null != adapter) {
            invalidateBullets(adapter);
        }
    }

    //START PAGER INDICATOR CONFIG

    public void invalidateBullets(PagerAdapter adapter) {
        final boolean hasSeparator = hasSeparator();
        mLayoutIndicator.setVisibility(hasSeparator ? INVISIBLE : VISIBLE);

        if (!hasSeparator) {
            initIndicator(adapter.getCount());
        }
        setIndicatorItem(mViewPager.getCurrentItem());
    }

    private void setIndicatorItem(int index) {
        if (!hasSeparator()) {
            setItemBullet(index);
        }
    }

    private boolean hasSeparator() {
        PagerAdapter pagerAdapter = mViewPager.getAdapter();
        return null != pagerAdapter && pagerAdapter.getCount() > offset;
    }

    private void setItemBullet(int selectedPosition) {
        Drawable drawableInactive = ContextCompat.getDrawable(this, R.drawable.inactive_dot);
        drawableInactive = wrapTintDrawable(drawableInactive, inactiveColorTint);
        Drawable drawableActive = ContextCompat.getDrawable(this, R.drawable.active_dot);
        drawableActive = wrapTintDrawable(drawableActive, activeColorTint);

        final int indicatorItemsCount = mLayoutIndicator.getChildCount();

        for (int position = 0; position < indicatorItemsCount; position++) {
            ImageView imageView = (ImageView) mLayoutIndicator.getChildAt(position);

            if (position != selectedPosition) {
                imageView.setImageDrawable(drawableInactive);

            } else {
                imageView.setImageDrawable(drawableActive);
            }
        }
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor(); // scale change since previous event

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));

            return true;
        }
    }

    private class RotateListener extends RotateGestureDetector.SimpleOnRotateGestureListener {
        @Override
        public boolean onRotate(RotateGestureDetector detector) {
            mRotationDegrees -= detector.getRotationDegreesDelta();
            return true;
        }
    }

    private class MoveListener extends MoveGestureDetector.SimpleOnMoveGestureListener {
        @Override
        public boolean onMove(MoveGestureDetector detector) {
            PointF d = detector.getFocusDelta();
            mFocusX += d.x;
            mFocusY += d.y;
            return true;
        }
    }

    //END PAGER INDICATOR CONFIG

}
