package fprieto.com.disclosurapp.ui.view.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.asksira.loopingviewpager.LoopingViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import fprieto.com.disclosurapp.R;
import fprieto.com.disclosurapp.ui.view.ItemSelectedListener;
import fprieto.com.disclosurapp.ui.view.adapter.PolaroidPagerAdapter;

/**
 * Created by fernando.prieto on 07/02/2018.
 */

public class PolaroidView extends FrameLayout {

    @BindView(R.id.asvp_polaroid_items)
    LoopingViewPager loopingViewPager;

    private PolaroidPagerAdapter pagerAdapter;
    private ArrayList<Integer> arrayPolaroidItems;
    private Context context;
    private ItemSelectedListener itemSelectedListener;


    public PolaroidView(@NonNull Context context) {
        super(context);
        this.context = context;
        init();
    }

    public PolaroidView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_polaroid_button, null);
        addView(view);
        ButterKnife.bind(this);
    }


    private void initViewPager() {
        pagerAdapter = new PolaroidPagerAdapter(arrayPolaroidItems, (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE), itemSelectedListener);
        loopingViewPager.setAdapter(pagerAdapter);
        loopingViewPager.setIndicatorPageChangeListener(new LoopingViewPager.IndicatorPageChangeListener() {
            @Override
            public void onIndicatorProgress(int selectingPosition, float progress) {
            }

            @Override
            public void onIndicatorPageChange(int newIndicatorPosition) {
                itemSelectedListener.onPolaroidItemChanged(arrayPolaroidItems.get(newIndicatorPosition));
            }
        });
    }

    public void setPolaroidElements(ArrayList<Integer> arrayPolaroidItems, ItemSelectedListener itemSelectedListener) {
        this.arrayPolaroidItems = arrayPolaroidItems;
        this.itemSelectedListener = itemSelectedListener;
        initViewPager();
    }


    public void resumeAutoScroll() {
        loopingViewPager.resumeAutoScroll();
    }

    public void pauseAutoScroll() {
        loopingViewPager.pauseAutoScroll();
    }
}
