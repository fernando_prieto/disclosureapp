package fprieto.com.disclosurapp.ui.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import fprieto.com.disclosurapp.R;
import fprieto.com.disclosurapp.ui.view.ItemSelectedListener;

/**
 * Created by fernandoprieto on 14/2/17.
 */

public class FacePagerAdapter extends PagerAdapter implements View.OnClickListener {

    Context mContext;
    LayoutInflater layoutInflater;

    private List<Integer> mFacesList;
    private ItemSelectedListener mItemSelectedListener;


    public FacePagerAdapter(Context context, ArrayList<Integer> arrayList, ItemSelectedListener itemSelectedListener ) {
        mContext = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mFacesList = arrayList;
        mItemSelectedListener = itemSelectedListener;
    }

    @Override
    public int getCount() {
        if (mFacesList != null) {
            return mFacesList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.item_viewpager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_item_face);
        imageView.setImageDrawable(ContextCompat.getDrawable(mContext, mFacesList.get(position)));
        itemView.setTag(mFacesList.get(position).toString());
        itemView.setOnClickListener(this);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public void onClick(View v) {
        mItemSelectedListener.onItemSelected(Integer.parseInt(v.getTag().toString()));
    }

}