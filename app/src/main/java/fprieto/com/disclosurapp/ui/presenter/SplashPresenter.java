package fprieto.com.disclosurapp.ui.presenter;


import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

public class SplashPresenter extends Presenter<SplashPresenter.View> {

    private static final int MAX_MINUTES = 10;
    private SharedPreferences mSharedPreferences;

    @Inject
    public SplashPresenter(@NonNull SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public String getLastTimeOpened() {
        return mSharedPreferences.getString("LAST_TIME", "");
    }

    public void setLastTimeOpened(String time) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("LAST_TIME", time);
        editor.commit();
    }

    public boolean isHigherThanMaxMins(String oldTime, String currentTime) {
        boolean result = false;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date oldDate = (Date) format.parse(oldTime);
            Date currDate = (Date) format.parse(currentTime);
            long minOld = oldDate.getTime();
            long minCurr = currDate.getTime();
            if ((minCurr - minOld) / (1000 * 60) > MAX_MINUTES) {
                result = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;

    }

    public interface View extends Presenter.View {

    }
}
