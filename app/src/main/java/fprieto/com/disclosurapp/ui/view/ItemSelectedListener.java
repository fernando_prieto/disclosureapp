package fprieto.com.disclosurapp.ui.view;

/**
 * Created by ferchurch on 8/12/16.
 */

public interface ItemSelectedListener {
    void onItemSelected(int id);

    void onPolaroidItemClicked();

    void onPolaroidItemChanged(int drawableId);
}
