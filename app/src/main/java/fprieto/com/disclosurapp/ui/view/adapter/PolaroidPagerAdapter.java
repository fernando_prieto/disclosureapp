package fprieto.com.disclosurapp.ui.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import fprieto.com.disclosurapp.R;
import fprieto.com.disclosurapp.ui.view.ItemSelectedListener;

/**
 * Created by fernandoprieto on 6/2/18.
 */

public class PolaroidPagerAdapter extends LoopingPagerAdapter<Integer> implements View.OnClickListener {

    LayoutInflater layoutInflater;

    private List<Integer> polaroidItems;
    private ItemSelectedListener itemSelectedListener;


    public PolaroidPagerAdapter(ArrayList<Integer> arrayList, LayoutInflater layoutInflater, ItemSelectedListener itemSelectedListener) {
        super(layoutInflater.getContext(), arrayList, true);
        this.layoutInflater = layoutInflater;
        this.itemSelectedListener = itemSelectedListener;
        polaroidItems = arrayList;
    }

    @Override
    protected View inflateView(int viewType, int listPosition) {
        return LayoutInflater.from(context).inflate(R.layout.item_polaroid_viewpager, null);
    }

    @Override
    protected void bindView(View convertView, int listPosition, int viewType) {
        ImageView imageView = (ImageView) convertView.findViewById(R.id.item_polaroid);
        imageView.setImageResource(polaroidItems.get(listPosition));
        imageView.setTag(polaroidItems.get(listPosition));
        imageView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        itemSelectedListener.onPolaroidItemClicked();
    }

}