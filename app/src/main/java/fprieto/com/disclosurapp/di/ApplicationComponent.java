package fprieto.com.disclosurapp.di;

import javax.inject.Singleton;

import dagger.Component;
import fprieto.com.disclosurapp.DisclosurappApplication;
import fprieto.com.disclosurapp.ui.view.BaseActivity;
import fprieto.com.disclosurapp.ui.view.SplashActivity;
import fprieto.com.disclosurapp.ui.view.UploadActivity;
import fprieto.com.disclosurapp.ui.view.UploadedActivity;

/**
 * Created by ferchurch on 21/01/2018.
 */

@Singleton
@Component(modules = {MainModule.class})
public interface ApplicationComponent {

    void inject(DisclosurappApplication disclosurappApplication);

    void inject(BaseActivity baseActivity);

    void inject(UploadActivity uploadActivity);

    void inject(UploadedActivity uploadedActivity);

    void inject(SplashActivity splashActivity);
}
