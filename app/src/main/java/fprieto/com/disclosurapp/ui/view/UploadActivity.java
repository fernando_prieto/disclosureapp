/*
 * Copyright (C) 2015 Karumi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fprieto.com.disclosurapp.ui.view;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fprieto.com.disclosurapp.DisclosurappApplication;
import fprieto.com.disclosurapp.R;
import fprieto.com.disclosurapp.ui.presenter.UploadPresenter;
import fprieto.com.disclosurapp.ui.view.widget.PolaroidView;

public class UploadActivity extends BaseActivity implements UploadPresenter.View, ItemSelectedListener {

    private static final String TAG = UploadActivity.class.getCanonicalName();
    private static final int REQUEST_SELECT_PICTURE = 0x01;

    @Inject
    UploadPresenter presenter;
    @BindView(android.R.id.content)
    ViewGroup rootView;
    @BindView(R.id.button_upload)
    PolaroidView mPolaroidView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getStatusBarColor();
        initializeDagger();
        initializePresenter();
        presenter.initialize();
        initializePolaroidView();
    }

    private void initializePolaroidView() {
        ArrayList<Integer> listPolaroidItems = new ArrayList<>();
        listPolaroidItems.add(R.drawable.ic_polaroid_item_pink);
        listPolaroidItems.add(R.drawable.ic_polaroid_item_green);
        listPolaroidItems.add(R.drawable.ic_polaroid_item_grey);
        listPolaroidItems.add(R.drawable.ic_polaroid_item_blue);
        listPolaroidItems.add(R.drawable.ic_polaroid_item_red);

        mPolaroidView.setPolaroidElements(listPolaroidItems, this);
    }

    private void getStatusBarColor() {
        if (getIntent() != null && getIntent().getBooleanExtra("IMAGE_SAVED", false) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.GREEN);
        }
    }

    @Override
    protected void onPause() {
        mPolaroidView.pauseAutoScroll();
        super.onPause();
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(data.getData());
                } else {
                    Toast.makeText(UploadActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "whatever.png";

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        UCrop.Options options = new UCrop.Options();
        options.setFreeStyleCropEnabled(true);
        uCrop.withOptions(options);

        uCrop.start(UploadActivity.this);
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    @OnClick(R.id.button_upload)
    public void clickUpload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPolaroidView.resumeAutoScroll();
    }

    private void initializePresenter() {
        presenter.setView(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_upload;
    }


    private void initializeDagger() {
        ((DisclosurappApplication) getApplication()).component().inject(this);
    }

    @Override
    public Context context() {
        return this;
    }

    public ContentResolver getContentResolver() {
        return getApplicationContext().getContentResolver();
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            UploadedActivity.startWithUri(UploadActivity.this, resultUri);
        } else {
            Toast.makeText(UploadActivity.this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
            Toast.makeText(UploadActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(UploadActivity.this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void setStatusbar(int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(color));
        }
    }

    private void setStatusbarColor(int drawableId) {
        switch (drawableId) {
            case R.drawable.ic_polaroid_item_red:
                setStatusbar(R.color.status_bar_red);
                break;
            case R.drawable.ic_polaroid_item_blue:
                setStatusbar(R.color.status_bar_blue);
                break;
            case R.drawable.ic_polaroid_item_grey:
                setStatusbar(R.color.status_bar_grey);
                break;
            case R.drawable.ic_polaroid_item_green:
                setStatusbar(R.color.status_bar_green);
                break;
            case R.drawable.ic_polaroid_item_pink:
                setStatusbar(R.color.status_bar_pink);
                break;
            case R.drawable.ic_polaroid_item_purple:
                setStatusbar(R.color.status_bar_purple);
                break;
        }
    }

    @Override
    public void onItemSelected(int id) {
    }

    @Override
    public void onPolaroidItemClicked() {
        clickUpload();
    }

    @Override
    public void onPolaroidItemChanged(int drawableId) {
        setStatusbarColor(drawableId);
    }
}