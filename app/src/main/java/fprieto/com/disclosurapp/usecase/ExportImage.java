package fprieto.com.disclosurapp.usecase;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Random;

import javax.inject.Inject;

import fprieto.com.disclosurapp.ui.view.SavingTempImageListener;

public class ExportImage {

    private final static String TEMP_FOLDER = "disclosureapp/temp";
    private final static String FINAL_FOLDER = "disclosureapp";
    private Context context;
    private boolean exported;

    @Inject
    public ExportImage(Context context) {
        this.context = context;
    }

    public void exportTempImage(Bitmap bitmap, boolean isSaving, SavingTempImageListener savingImageListener) {
        String path;
        if (isSaving) {
            path = new String(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES) + File.separator + FINAL_FOLDER + File.separator);
        } else {
            path = new String(Environment.getExternalStorageDirectory() + File.separator + TEMP_FOLDER + File.separator);
        }

        File fileDirectory = new File(path);
        fileDirectory.mkdirs();
        final Random random = new Random();
        String name = isSaving ? "image" : "temporary" + random.nextInt(1000);
        File outputFile = new File(fileDirectory, name + ".png");
        OutputStream fo;
        exported = false;
        try {
            fo = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fo);
            fo.flush();
            fo.close();
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    outputFile.getAbsolutePath(), name,
                    "Image exported" + getDateTodayFormatted());
            exported = true;
        } catch (IOException e) {
            e.printStackTrace();
            exported = false;
        } catch (NullPointerException n) {
            n.printStackTrace();
            exported = false;
        }
        savingImageListener.onImageSaved(exported, outputFile.getAbsolutePath(), isSaving);
    }

    private String getDateTodayFormatted() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.toString();
    }


}
