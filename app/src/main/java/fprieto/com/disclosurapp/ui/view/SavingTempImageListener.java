package fprieto.com.disclosurapp.ui.view;

/**
 * Created by ferchurch on 8/12/16.
 */

public interface SavingTempImageListener {
    void onImageSaved(boolean result,String path,boolean isSaving);
}
