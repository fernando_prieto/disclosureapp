package fprieto.com.disclosurapp.ui.view;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fprieto.com.disclosurapp.DisclosurappApplication;
import fprieto.com.disclosurapp.R;
import fprieto.com.disclosurapp.ui.presenter.SplashPresenter;

public class SplashActivity extends BaseActivity implements SplashPresenter.View {

    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    @BindView(R.id.animation_image)
    ImageView mAnimationImage;
    @Inject
    SplashPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initializeDagger();
        initializePresenter();
        mPresenter.initialize();
        checkLoadAnimationNeeded();
    }

    private void initializeDagger() {
        ((DisclosurappApplication) getApplication()).component().inject(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    private void checkLoadAnimationNeeded() {
        String lastTimeOpened = mPresenter.getLastTimeOpened();
        Date newDate = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentTime = formatter.format(newDate);
        if (mAnimationImage != null && lastTimeOpened.isEmpty() || mPresenter.isHigherThanMaxMins(lastTimeOpened, currentTime)) {
            loadAnimation();
        } else {
            goToUploadActivity();
        }
        mPresenter.setLastTimeOpened(currentTime);


    }

    private void initializePresenter() {
        mPresenter.setView(this);
    }

    public void loadAnimation() {

        final AnimationDrawable frameAnimation = (AnimationDrawable) mAnimationImage.getDrawable();
        frameAnimation.setCallback(mAnimationImage);
        frameAnimation.setVisible(true, true);

        new CountDownTimer(2900, 50) {

            public void onTick(long millisUntilFinished) {
                frameAnimation.start();
            }

            public void onFinish() {
                frameAnimation.stop();
                launchSplashAndFinish();
            }
        }.start();
    }

    private void launchSplashAndFinish() {
        //TODO: IMPLEMENT PRESENTER TO DO THESE OPERATIONS
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                goToUploadActivity();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, AUTO_HIDE_DELAY_MILLIS);
    }

    private void goToUploadActivity() {
        finish();
        Intent intent = new Intent(getApplicationContext(), UploadActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.animation_fade_in, R.anim.animation_fade_out);
    }


    @Override
    public void onBackPressed() {
    }

}
